<div class="jumbotron">
            <h2 class="text-center">Colaboradores Ativos "<?= date("d/m/Y") ?>"</h2>
        </div>
<table class="table table">
    <thead class="thead-dark">
        <tr>
            <th>Loja</th>
            <th>Funcionário</th>
            <th>Situação</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr class="alert alert-success">
            <td>SV - Shopping</td>
            <td>Alessandro</td>
            <td>Presente</td>
        </tr>
        <tr class="alert alert-danger">
            <td>SV - Rua</td>
            <td>Bruno</td>
            <td>Presente</td>
        </tr>
        <tr>
            <td>SV - Rua</td>
            <td>Manoel</td>
            <td>Presente</td>
        </tr>
        <tr>
            <td>Santos</td>
            <td>Michael</td>
            <td>Presente</td>
        </tr>
    <tbody>
</table>